module UsersHelper
  def range
    [@user.daterange.gsub(',', '\', \'').gsub(/\A/, '\'').gsub(/\z/, '\'').html_safe]
  end
end

