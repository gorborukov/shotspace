class Autocompletes
  def initialize(app)
    @app = app
  end
  
  def call(env)
    if env["PATH_INFO"] == "/autocompletes"
      request = Rack::Request.new(env)
      terms = Autocomplete.terms_for(request.params["term"])
      [200, {"Content-Type" => "appication/json"}, [terms.to_json]]
    else
      @app.call(env)
    end
  end
end