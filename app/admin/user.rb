ActiveAdmin.register User do

  
  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  permit_params :username, :email, :password, :current_password, :nickname, :provider, :url, :avatar, :daterange, :website, :city_id, :region_id, :phone, :dayprice, :hourprice, :photos_array => [], :category_ids => []
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end
  
end
