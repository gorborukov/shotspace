json.array!(@cities) do |city|
  json.extract! city, :id, :name, :user_id, :region_id, :country_id
  json.url city_url(city, format: :json)
end
