json.photostream(@user.photos) do |json, photo|
  json.image photo.image.url(:medium)
end