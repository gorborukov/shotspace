# encoding: utf-8
class BookingsController < ApplicationController
  def index
  	redirect_to not_found_path
  end

  def edit
  	redirect_to not_found_path
  end

  def show
  	redirect_to not_found_path
  end

  def new
  	redirect_to not_found_path
  end

  def create
    @booking = Booking.new(booking_params)
    respond_to do |format|
      if @booking.save
        UserMailer.send_private(@booking).deliver 
        format.html { redirect_to user_path(@booking.user_id), notice: 'Спасибо, заявка отправлена фотографу!' }
      else
        format.html { redirect_to :back, notice: 'Похоже что вы неправильно ответили на вопрос в последнем поле!'  }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_booking
      @booking = Booking.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def booking_params
      params.require(:booking).permit(:name, :email, :phone, :daterange, :additional, :user_id, :humanizer_answer, :humanizer_question_id)
    end
end
