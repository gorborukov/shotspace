class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  before_filter :set_cache_headers

  protect_from_forgery with: :exception
  def after_sign_in_path_for(resource)
    user_path(current_user)
  end

  def after_update_path_for(resource)
    user_path(current_user)
  end

  rescue_from ActiveRecord::RecordNotFound do |exception|
    render_error 404
  end

  def render_error(status)
	respond_to do |format|
	  format.html { render :template => "pages/not_found", :status => status}
	  format.all { render :nothing => true, :status => status }
	end
  end
 
  def set_cache_headers
    response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
    response.headers["Pragma"] = "no-cache"
    response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
  end
end
