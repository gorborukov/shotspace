class PhotosController < ApplicationController

  def create
    photo = Photo.create(:image => params[:file], :user_id => current_user.id)
    render :json => {:success => true}
  end
  
  def destroy
    @photo = Photo.find(params[:id])
    @photo.destroy
    respond_to do |format|
      format.html { redirect_to :back }
      format.json { head :no_content }
    end
  end

  def edit
    redirect_to not_found_path
  end
  
  def new
    redirect_to not_found_path
  end
 
end
