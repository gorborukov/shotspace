class CategoriesController < ApplicationController
  before_action :set_category, only: [:show, :edit, :update, :destroy]
  layout :choose_layout

  # GET /categories
  # GET /categories.json
  def index
    @categories = Category.all
  end

  # GET /categories/1
  # GET /categories/1.json
  def show
    @category = Category.find(params[:id])
    @city = City.find(params[:city_id])
    @users = Category.find(params[:id]).users.where(:city_id => params[:city_id]).page(params[:page]).per_page(10)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_category
      @category = Category.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def category_params
      params.require(:category).permit(:name)
    end

    def choose_layout
      case action_name
      when 'show'
        'index'
      else
        'application'
      end
    end
end
