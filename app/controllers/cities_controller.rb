class CitiesController < ApplicationController
  before_action :set_city, only: [:show, :edit, :update, :destroy]
  layout :choose_layout


  def show
    @search = User.where(:city_id => @city).order('created_at DESC').search(params[:q])
    @users = @search.result.page(params[:page]).per_page(10)
    @categories = Category.all
  end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_city
      @city = City.find(params[:id])
    end

    def choose_layout
      case action_name
      when 'show'
        'index'
      else
        'application'
      end
    end
end
