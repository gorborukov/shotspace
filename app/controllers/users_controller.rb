class UsersController < ApplicationController
  layout :choose_layout
  around_filter :catch_not_found

  def index
    @search = User.order('created_at DESC').search(params[:q])
    @users = @search.result.page(params[:page]).per_page(10)
  end

  def show
    @user = User.find(params[:id])
    @photos = @user.photos.page(params[:page]).per_page(6)
    respond_to do |format|
      format.html
      format.js # add this line for your js template
    end
  end
  
  def photos
    @user = User.find(params[:id])
    @photos = @user.photos
  end

  def profile
    @user = current_user
  end
  
  def upload
    if current_user
      @user = current_user
      @photos = @user.photos
    else
      redirect_to not_found_path
    end
  end
  
  def booking
    @booking = Booking.new
  end

  def bookings
    if current_user 
      @bookings = Booking.where(:user_id => current_user)
    else
      redirect_to not_found_path
    end
  end

  def edit
    if current_user
  	  @user = current_user
    else
      redirect_to not_found_path
    end

    def get_drop_down_options
      val = params[:region]
      #Use val to find records
      options = City.collect{|x| "'#{x.id}' : '#{x.label}'"}    
      render :text => "{#{options.join(",")}}" 
    end
  end
  
  def update
    @user = current_user 
    if user_params[:password].blank?
      user_params.delete :password
      user_params.delete :password_confirmation
    end
    
    if @user.update(user_params)
      sign_in @user, :bypass => true
      redirect_to @user, notice: 'User was successfully updated.'
    else
      render action: 'edit'
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:username, :email, :password, :current_password, :nickname, :provider, :url, :avatar, :daterange, :website, :city_name, :city_id, :region_id, :phone, :dayprice, :hourprice, :photos_array => [], :category_ids => [])
    end

    def needs_password?(user, params)
      params[:password].present?
    end

    def after_update_path_for(resource)
      user_path(current_user)
    end

    def choose_layout
      case action_name
      when 'show'
        'user'
      when 'edit'
        'user_edit'
      when 'index', 'booking', 'bookings', 'upload'
        'index'
      when 'photos'
        'photos'
      else
        'application'
      end
    end

    def catch_not_found
      yield
    rescue ActiveRecord::RecordNotFound
      redirect_to not_found_path
    end
end