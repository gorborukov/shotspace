class PagesController < ApplicationController
  layout :choose_layout

  def welcome
  end

  def autocomplete
    cities = City.search(params['term'])
    render :json => cities
  end

  def not_found
  end
  
  def promo
  end

  private
  
  def choose_layout
    case action_name
  	when 'welcome', 'autocomplete', 'not_found'
  		'welcome'
    when 'promo'
      'promo'
  	else
  		'application'
  	end
  end
end
