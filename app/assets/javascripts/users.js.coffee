jQuery ->
  loaded_model = $('#user_city_id :selected').text()
  if loaded_model.length < 1
    $('#user_city_id').parent().hide()
  cities = $('#user_city_id').html()
  $('#user_region_id').change ->
    region = $('#user_region_id :selected').text()
    escaped_region = region.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
    options = $(cities).filter("optgroup[label='#{escaped_region}']").html()
    
    if options
      $('#user_city_id').html(options)
      $('#user_city_id').parent().show()
    else
      $('#user_city_id').empty()
      $('#user_city_id').parent().hide()
  $("#user_region_id").trigger "change"

jQuery ->

  if $('.pagination').length
    $(window).scroll ->
      url = $('.pagination .next_page').attr('href')
      if url && $(window).scrollTop() > $(document).height() - $(window).height() - 100
        $('.pagination').text("Загружаю еще...")
        $.getScript(url)
    $(window).scroll()



