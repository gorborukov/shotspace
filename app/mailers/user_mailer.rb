# encoding: utf-8
class UserMailer < ActionMailer::Base

  default from: Settings.mailer.default_from,
          to: Settings.mailer.default_to
  
  def new_user_message(current_user)
    unless current_user.nil?
  	  mail(to: current_user.email, subject: "Привет от Shotspace!", body: "Привет, регистрация прошла успешно! Теперь, чтобы клиенты не обходили вас стороной, рекомендуем заполнить профиль и загрузить фотографии в портфолио.")
    else
      mail(subject: "Welcome Email Fail", body: "Bye!")
    end
  end

  def send_private(booking_params)
    @booking_params = booking_params
    mail(to: User.where(:id => @booking_params.user_id).first.email, subject: "New booking request Shotspace")
  end
  
end