class Photo < ActiveRecord::Base
  belongs_to :user
  has_attached_file :image, 
  :styles => { :original => "900x600>", :pre_medium => "360x238>", :medium => "530x350>", :list => "218x145>", :small => "120x80>", :thumb => "100x100#", :med_thumb => "145x145#", :square => "280x280#" },
  :convert_options => { :pre_medium => '-quality 75', :medium => '-quality 75', :list => '-quality 75'},
  :default_url => "/assets/no-photo.svg"
  
  validates_attachment_content_type :image, :content_type => /^image\/(png|gif|jpeg)/
end
