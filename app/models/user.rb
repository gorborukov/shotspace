class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable, :omniauth_providers => [:vkontakte]

  belongs_to :region
  belongs_to :city
  has_many :photos
  has_many :user_categories
  has_many :categories, :through => :user_categories
  has_many :bookings

  def city_name
    city && city.name
  end

  def city_name=(value)
    self.city = City.find_by_name(value)
    self.city ||= City.new(:name => value)
  end


  def self.find_for_vkontakte_oauth access_token
    if user = User.where(url: access_token.info.urls.Vkontakte).first
      user
    else
      User.create!(provider: access_token.provider,
          url: access_token.info.urls.Vkontakte,
          username: access_token.info.name,
          nickname: access_token.uid,
          email: access_token.uid.to_s + '@vk.com',
          password: Devise.friendly_token[0,20]) 
    end
  end
  
  validates_each :photos do |user, attr, value|
    user.errors.add attr, "Не более 100 изображений для одного пользователя" if user.photos.size >= 101
  end
  
  NUM_REGEX = /\A[0-9]+\z/ || //

  validates :email, :presence => true, 
                    :length => {:maximum => 50},
                    :uniqueness => true,
                    :format => {:with => /@/}
  validates :username, :length => {:maximum => 50}
  validates :phone, :length => {:maximum => 20}
  validates :website, :length => {:maximum => 30}
  validates :dayprice, :length => {:maximum => 7}
  validates :hourprice, :length => {:maximum => 7}

  def photos_array=(array)
    array.each do |file|
      photos.build(:image => file)
    end
  end

  has_attached_file :avatar, :styles => { :medium => "300x300#", :thumb => "145x145#", :micro => "50x50#" }, :default_url => "/assets/missing-avatar.png"
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/
  
  # message after signup
  after_create :send_admin_mail
  def send_admin_mail
    UserMailer.new_user_message(self).deliver
  end

  private

end
