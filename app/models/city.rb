class City < ActiveRecord::Base
  belongs_to :region
  has_many :users
  has_many :categories

  after_save :add_to_soulmate
  before_destroy :remove_from_soulmate

  private

  def add_to_soulmate
    loader = Soulmate::Loader.new("city")
    loader.add("term" => name, "id" => self.id, "data" => { "region" => region_id})
  end

  def remove_from_soulmate
    loader = Soulmate::Loader.new("city")
    loader.remove("id" => self.id)
  end

  def self.search(term)
    cities = Soulmate::Matcher.new("city").matches_for_term(term)
    cities.collect { |c| { "link" => c["id"], "value" => c["term"], "region" => c["data"]["region"] } }
  end
end
