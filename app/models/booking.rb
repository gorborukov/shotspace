class Booking < ActiveRecord::Base
  belongs_to :user

  NUM_REGEX = /\A[0-9]+\z/ || //

  validates :email, :presence => true, 
                    :length => {:maximum => 50},
                    :format => {:with => /@/}
  validates :name, :length => {:maximum => 50}
  validates :phone, :length => {:maximum => 20}
  validates :daterange, :length => {:maximum => 100}
  validates :additional, :length => {:maximum => 250}

  include Humanizer
  require_human_on :create
end
