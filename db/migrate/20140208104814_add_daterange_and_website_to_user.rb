class AddDaterangeAndWebsiteToUser < ActiveRecord::Migration
  def change
    add_column :users, :daterange, :string
    add_column :users, :website, :string
  end
end
