class CreateBookings < ActiveRecord::Migration
  def change
    create_table :bookings do |t|
      t.string :name
      t.string :email
      t.string :phone
      t.string :daterange
      t.text :additional
      t.integer :user_id

      t.timestamps
    end
  end
end
