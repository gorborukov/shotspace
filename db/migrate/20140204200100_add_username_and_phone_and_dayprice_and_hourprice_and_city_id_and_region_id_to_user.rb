class AddUsernameAndPhoneAndDaypriceAndHourpriceAndCityIdAndRegionIdToUser < ActiveRecord::Migration
  def change
    add_column :users, :username, :string
    add_column :users, :dayprice, :integer
    add_column :users, :hourprice, :integer
    add_column :users, :city_id, :integer
    add_column :users, :region_id, :integer
  end
end
