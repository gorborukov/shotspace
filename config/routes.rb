Shotspace::Application.routes.draw do

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  resources :bookings

  mount Soulmate::Server, :at => '/sm'

  get "omniauth_callbacks/vkontakte"
  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks" }

  resources :photos

  resources :categories

  get "pages/welcome" => "pages#welcome"
  get "pages/promo" => "pages#promo"
  get "autocomplete" => "pages#autocomplete", :as => 'autocomplete'
  get "profile" => "users#profile"

  get 'users/:id/booking' => "users#booking", :as => 'user_booking'
  get 'users/:id/photos' => "users#photos", :as => 'user_photos'
  get 'users/:id/upload' => "users#upload", :as => 'user_upload'
  get 'my/bookings' => "users#bookings", :as => 'my_bookings'
  
  get 'not_found' => 'pages#not_found', as: :not_found

  resources :cities
  resources :users

  resources :cities do
    collection do
      get 'autocomplete'
    end
  end

  resources :cities do
    resources :categories do
      resources :users
    end
  end

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'pages#welcome'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
