namespace :db do
  desc "Fill database with sample data"
  task populate: :environment do
    password = "password"

    50.times do |i|
      username = Faker::Name.name
      email = Faker::Internet.email
      encrypted_password = User.new(:password => password).encrypted_password
      # rest of your code here   
      User.create!( username: username, email: email, password: password, password_confirmation: password)   
    end


  end
end